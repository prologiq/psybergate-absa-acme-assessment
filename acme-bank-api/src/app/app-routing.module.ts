import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
// Define our Routes: /accounts to view accounts
import { AccountListComponent } from './components/account-list/account-list.component';

const routes: Routes = [
  {path: 'accounts', component: AccountListComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
