import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.base.css', './app.component.css']
})
export class AppComponent {
  title = 'acme-bank-api';
}
