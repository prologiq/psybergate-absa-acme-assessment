# ACME Bank API

This is a general `README ME` file that has a list of task and critical information for the application(including startup instructions). 
## Key Notes

### Modules
We need to install necessary modules: `express`, `sequelize`, `mysql2` and `body-parser`

```bash
npm install express sequelize mysql2 body-parser cors --save
```

### Database Credentials
- Server: 
- Username: 
- Password: 
- Database Name: 

### Setup Instructions(Please note that all these commands need to be run from the `acme-bank-api` directory)
- Install the prerequisite libraries by running `npm install`
- Start up the server by running `ng serve`

### User Status'
- CRT: Created
- PND: Pending
- ACV: Active
- SUP: Suspended
- DLT: Deleted

### Application Users(Pre-Defined)
These are just some sample users created in the `user_details` table in the database. The table is mainly responsible for user management with basic CRUD functionality

| Username  | Password | User Type |
| ------------- | ------------- | ------------- |
| logiqadmin  | LogiQAdmin2021  | Administrator  |
| sammysa  | Sammy1234567  | Savings  |
| phatone  | Phat1234567  | Current  |


### Transactions
This table records every transactions as they occur. The transactions that will be recorded will be:
- Deposit
- Withdrawal

Transaction status'
- Complete: Can either be a withdrawal or deposit being completed
- Declined: Usually a withdrawl that is attempted but not completed due to some conditions not being met